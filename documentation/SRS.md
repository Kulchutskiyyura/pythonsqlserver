1 General description<br>
This project is created to facilitate employees management.
It contain following functionality:
* adding\deleting department
* adding\deleting employee from department
* rename department
* show departments list
* show employees attached to department
* rename department
* adding\deleting employee
* change employee data (name,surname,data of birth,salary, department)

2 Users<br>

2.1 Users permission<br>
There are two user roles in system: Admin and Standard.
Standard user can only view information about departments and employees.
Admin has all permission of  Standard  user and additionally can delete\edit employee and department.
Also Admin can create new user.<br>

2.2 User creation<br>
**Only Admin can create new user!**<br>
Press `add user` link at top left corner. After that you will be redirected
to form:<br>
![create user](create_user.png)

Enter user login to name field. It must be no shorter then 3 characters and  no longer then 25 characters.<br>
It can contain both alphabetical and numerical characters. Each user login must be unique.

Enter user password to password field. It must be no shorter then 3 characters and no longer then 25 characters.<br>
It can contain both alphabetical and numerical characters. 

In repeat password field enter the same password one more time. 

In access level filed choose user role(Standard or Admin).

2.3 Edit User<br>

Press `edit user` link at top left corner. After that you will be redirected
to form:<br>
![edit](edit_user.png)<br>
Enter new user login to name field. It must be no shorter then 3 characters and  no longer then 25 characters.<br>
It can contain both alphabetical and numerical characters. Each user login must be unique.

Enter user password to password field. It must be no shorter then 3 characters and no longer then 25 characters.<br>
It can contain both alphabetical and numerical characters. 

In repeat password field enter the same password one more time. 


2.4 Log In<br>
Only log in user has access to the site. Non log in user will be automatically redirect to the following form: <br>
![log in](log_in.png)<br>
Enter your login to name field.

Enter your password to password field.

If you want system remember you, click remember me chechbox.

2.5 Log Out<br>

If you want to log out click `log out` link at top left corner.

3  Department<br>

3.1 Create Department<br>
**Only Admin can create new department!**<br>
Press `add new department` button at the main page. You will be redirected to the form.<br>
![add new department](add_deppartment.png)
<br>
Enter department name to the name field. Department names can repeat, but it is not recommended.
Name must be no shorter then 3 characters and  no longer then 25 characters.<br>
It can contain both alphabetical and numerical characters

3.2 Delete Department <br>
**Only Admin can delete department!**<br>

Click to the `delete department` link, which is opposite to the depertment, you want to delete.

**Remember! All user that are attached to the department wiil be also deleted**

3.2 Edit Department <br>
**Only Admin can edit department!**<br>

Click to the `edit department` link, which is opposite to the depertment, you want to edit.
You will be redirected to the form.<br>
![edit department](edit_department.png)<br>
Enter new department name. It must be no shorter then 3 characters and no longer then 25 characters.<br>
It can contain both alphabetical and numerical characters.

4 Employee<br>

4.1 View Employees list

Click to the `show employees` link, that is opposite to the department, which employees you want to see.<br>
After that you will be redirected to the page, in which you can see following information about employees:
name, surname, date of birth, salary

4.2 Create Employee<br>
**Only Admin can create employee!**<br>

Go to employees list(use 4.1 View Employees list). Click to the `create new employee` button. After that you will be redirected to the form.<br>
![add employee](add_employee.png)<br>

Enter employee name to the name field. It must be no shorter then 3 characters and no longer then 25 characters.

Enter employee surname to the name field. It must be no shorter then 3 characters and no longer then 25 characters.

Enter employee salary to the salary field. Only integer number can be put in this field. Because of that we recommend you to enter salary in cent (100 dollars = 10000 cent).

Enter employye date of birth to date of birth filed. Date of birth must be put in the following format `2001-06-05`.

Choose employee department using department field. You can not add employee who is not atteched to the department.

4.3 Delete Employee<br>
**Only Admin can delete employee!**<br>

Go to employees list(use 4.1 View Employees list). Click to the `delete employee data` link which is opposite to the employee, you want to delete.

4.4 Edit Employee<br>
**Only Admin can edit employee!**<br>
 
Go to employees list(use 4.1 View Employees list). Click to the `edit employee data` link which is opposite to the employee, you want to edit.ter that you will be redirected to the form.<br>
![edit employee](edit_employee.png)<br>
Enter new employee name to the name field. It must be no shorter then 3 characters and no longer then 25 characters.

Enter new employee surname to the name field. It must be no shorter then 3 characters and no longer then 25 characters.

Enter new employee salary to the salary field. Only integer number can be put in this field. Because of that we recommend you to enter salary in cent (100 dollars = 10000 cent).

Enter new  employee date of birth to date of birth filed. Date of birth must be put in the following format `2001-06-05`.

Choose new employee department using department field. You can not add employee who is not attached to the department.



