1 General description<br>
This project is created to facilitate employees management.
It contain following functionality:
* adding\deleting department
* adding\deleting employee from department
* rename department
* show departments list
* show employees attached to department
* rename department
* adding\deleting employee
* change employee data (name,surname,data of birth,salary, department)<br>

If you whant to learn more about how to use this application go to documentation\SRC.md

**instalation**

**make sure you have pip and python instaled**

**create virtual enviroment**

 python3 -m venv env

 source env/bin/activate

 cd env

 **clone project from gitlab**

 git clone https://gitlab.com/Kulchutskiyyura/courseproject.git 

 **or**

 git clone git@gitlab.com:Kulchutskiyyura/courseproject.git

**install all needed packeges**

 pip install DepartmnetHelper==1.0.0

 for additional instruction go to https://pypi.org/project/DepartmnetHelper/1.0.0/

**create and configure database**

sudo mysql

create database  database_name;

use database_name;

go to application/__init__.py
in line 9: 
 `DATABSE_URI = 'mysql+mysqlconnector://{user}:{password}@{server}/{database}'.format(user='Yura', password='1111', server='localhost', database='python_project_db')`<br>
change user password server database to your  user password server and database

**migration init**<br>

flask db init

flask db migrate -m "Initial migration."

 flask db upgrade

 **User creation**

 By default system has one admin. His name is User and password is 1111

 **Please edit this user as soon as you log in to system**
 You can learn how to do it in documentation\SRC.md

 **run application**

 cd courseproject

 export FLASK_APP=run for linux

 **or**

 set  FLASK_APP=run for Windows

 flask run

 **run test**

 cd courseproject\appliaction

 coverage run -m pytest

 **show test coverage**

  coverage report forms.py models.py views.py services.py