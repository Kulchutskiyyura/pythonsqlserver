import os

from flask import Flask, request, render_template, redirect, url_for, redirect, flash
from flask_login import login_user, login_required, current_user, logout_user

from application import app
from application.forms import AddEmployeeForm
from application.forms import CreateDepartment
from application.forms import *
from application import services
from application.models import User

from application import login_manager

login_manager.login_view = "login"

current_personal_account = 0
current_aha = 0


@app.route('/', methods=['GET'])
@login_required
def main():
    """
    Main page

    Show start page where user can see department list
    
    User must be authorized to get access

    return show_department template with departments list as parametr

    """
    ahas = services.get_ahas()
    return render_template('show_aha.html', ahas=ahas)


@app.route('/add_aha', methods=['POST', 'GET'])
@login_required
def add_aha():
    """
    Show page, where user can add department
    User must be authorized to get access

    return add_department template with  CreateDepartment form as paramtr in case of Get respone

    return redirect to main page in case of Post respone
    """
    form = AddAHA(request.form)
    if request.method == "POST":
        if form.validate():
            services.add_aha(name=form.name.data, location=form.location.data)
            return redirect(url_for('main'))
        return "error in form"
    return render_template('add_aha.html', form=form)


@app.route('/edit_aha/<int:aha_id>', methods=['POST', 'GET'])
@login_required
def edit_aha(aha_id: int):
    """
    Show page, where user can edit department
    User must be authorized to get access

    input parametrs:

    dep_id:int - id of departmnet, that will be edited

    return edit_department template with  CreateDepartment form as paramtr in case of Get respone

    return redirect to main page in case of Post respone
    """
    form = AddAHA(request.form)
    if request.method == "POST":
        if form.validate():
            services.edit_aha(id=aha_id, name=form.name.data, location=form.location.data)
            return redirect(url_for('main'))
        return "error in form"
    return render_template('edit_aha.html', form=form)


@app.route("/delete_department/<int:aha_id>", methods=["GET"])
@login_required
def delete_aha(aha_id: int):
    """
    Delete department 
    User must be authorized to get access

    input parametrs:

    dep_id:int - id of departmnet, that will be deleted

    return redirect to main page

    """
    services.delete_aha(aha_id)
    return redirect(url_for('main'))


@app.route('/login', methods=['POST', 'GET'])
def login():
    """
    Show page, where user can log in

    If there are no users in database create new default user

    return login template with  LogIn form as paramtr in case of Get respone

    return redirect to main page in case of Post respone

    """

    form = LogIn(request.form)
    if request.method == "POST":
        if form.validate():
            admin = services.get_admin_by_login(login=form.name.data, password=form.password.data)
            if admin is not None:
                login_user(admin, remember=form.remember.data)
                return redirect(request.args.get('next') or url_for('main'))
        return "invalid data"
    return render_template('login.html', form=form)


@login_manager.user_loader
def load_user(user_id: int):
    """
    Set current user
    input parametrs:

    user_id:int - id of current user

    return current user
    """
    return services.get_admin_by_id(int(user_id))


@app.route("/logout")
@login_required
def logout():
    """
    Log out user
    User must be authorized to get access

    return redirect to main page
    """
    logout_user()
    return redirect(url_for('main'))


@app.route('/add_client', methods=['POST', 'GET'])
@login_required
def add_client():
    """
    Show page, where user can add employee
    User must be authorized to get access

    
    input parametrs:

    dep_id:int - id of departmnet, which user is atteched to

    return add_employee  template with  AddEmployeeForm form as paramtr in case of Get respone

    return redirect to show_employee template with dept_id as paramtr in case of Post respone
    """
    form = AddClient(request.form)
    form.aha.choices = [
        *zip([i.id for i in services.get_ahas()], [i.name for i in services.get_ahas()])]
    if request.method == "POST" and form.validate():
        services.add_client(
            name=form.name.data,
            surname=form.surname.data,
            address=form.address.data,
            aha_id=form.aha.data
        )
        return redirect(url_for('show_client', aha_id=form.aha.data))
    return render_template('add_client.html', form=form)


@app.route('/show_client/<int:aha_id>', methods=['GET'])
@login_required
def show_client(aha_id: int):
    """
    Show page, where user can see employee
    User must be authorized to get access

    input parametrs:

    dep_id:int - id of departmnet, which user is atteched to

    return show_employee  template with  employees list and dep_id as paramtrs

    """
    global current_aha
    current_aha = aha_id
    clients = services.get_client(int(aha_id))
    print(clients)
    return render_template('show_client.html', clients=clients, current_aha=current_aha)


@app.route("/edit_client/<int:id>", methods=["GET", "POST"])
@login_required
def edit_client(id: int):
    """
    Show page, where user can edit employee
    User must be authorized to get access

    
    input parametrs:

    dep_id:int - id of departmnet, which user is atteched to
    id:int - id of employee that will be edited

    return edit_employee  template with  AddEmployeeForm form as paramtr in case of Get respone

    return redirect to show_employee template with dept_id as paramtr in case of Post respone
    """
    form = AddClient(request.form)
    form.aha.choices = [
        *zip([i.id for i in services.get_ahas()], [i.name for i in services.get_ahas()])]
    if request.method == "POST" and form.validate():
        services.update_client(
            id=id,
            name=form.name.data,
            surname=form.surname.data,
            address=form.address.data,
            aha_id=form.aha.data
        )
        return redirect(url_for('show_client', aha_id=form.aha.data))
    return render_template('edit_client.html', form=form)


@app.route("/delete_employee/<int:aha_id>/<int:id>", methods=["GET"])
@login_required
def delete_client(aha_id: int, id: int):
    """
    Delete employee
    User must be authorized to get access

    input parametrs:

    dep_id:int - id of departmnet, that will be deleted
    id:int - id of employee that will be deleted

    return redirect to main page
    """
    services.delete_client(id)
    return redirect(url_for('show_client', aha_id=aha_id))


@app.route('/show_personal_account/<int:client_id>', methods=['GET'])
@login_required
def show_personal_account(client_id: int):
    """
    Show page, where user can see employee
    User must be authorized to get access

    input parametrs:

    dep_id:int - id of departmnet, which user is atteched to

    return show_employee  template with  employees list and dep_id as paramtrs

    """
    personal_accounts = services.get_personal_account(int(client_id))
    return render_template('show_personal_account.html', personal_accounts=personal_accounts, current_aha=current_aha)


@app.route("/edit_personal_account/<int:id>", methods=["GET", "POST"])
@login_required
def edit_personal_account(id: int):
    """
    Show page, where user can edit employee
    User must be authorized to get access


    input parametrs:

    dep_id:int - id of departmnet, which user is atteched to
    id:int - id of employee that will be edited

    return edit_employee  template with  AddEmployeeForm form as paramtr in case of Get respone

    return redirect to show_employee template with dept_id as paramtr in case of Post respone
    """
    form = AddPersonalAcount(request.form)
    form.client.choices = [
        *zip([i.id for i in services.get_all_client()], [i.name for i in services.get_all_client()])]
    if request.method == "POST" and form.validate():
        services.update_personal_account(
            id=id,
            address=form.address.data,
            balance=form.balance.data,
            is_deptor=form.is_deptor.data,
            client_id=form.client.data
        )
        return redirect(url_for('show_personal_account', client_id=form.client.data))
    return render_template('edit_personal_account.html', form=form)


@app.route('/add_personal_account', methods=['POST', 'GET'])
@login_required
def add_personal_account():
    """
    Show page, where user can add employee
    User must be authorized to get access


    input parametrs:

    dep_id:int - id of departmnet, which user is atteched to

    return add_employee  template with  AddEmployeeForm form as paramtr in case of Get respone

    return redirect to show_employee template with dept_id as paramtr in case of Post respone
    """
    form = AddPersonalAcount(request.form)
    form.client.choices = [
        *zip([i.id for i in services.get_all_client()], [i.name for i in services.get_all_client()])]
    if request.method == "POST" and form.validate():
        services.add_personal_account(
            address=form.address.data,
            balance=form.balance.data,
            is_deptor=form.is_deptor.data,
            client_id=form.client.data
        )
        return redirect(url_for('show_personal_account', client_id=form.client.data))
    return render_template('add_personal_account.html', form=form)


@app.route("/delete_personal_account/<int:client_id>/<int:id>", methods=["GET"])
@login_required
def delete_personal_account(client_id: int, id: int):
    """
    Delete employee
    User must be authorized to get access

    input parametrs:

    dep_id:int - id of departmnet, that will be deleted
    id:int - id of employee that will be deleted

    return redirect to main page
    """
    services.delete_personal_account(id)
    return redirect(url_for('show_personal_account', client_id=client_id))


@app.route("/add_user", methods=['POST', 'GET'])
@login_required
def add_user():
    """
    Add user
    User must be authorized to get access

    return add_user  template with   CreateUser form as paramtr in case of Get respone

    return redirect to main page with dept_id as paramtr in case of Post respone
    """
    form = CreateUser(request.form)
    if request.method == "POST" and form.validate():
        user_with_the_same_name = services.get_user_by_name(form.name.data)
        if form.password.data == form.password_check.data and not user_with_the_same_name:
            services.create_user(
                name=form.name.data,
                password=form.password.data,
                access_level=form.access_level.data
            )
            return redirect(url_for('main'))
    return render_template('add_user.html', form=form)


@app.route("/edit_user", methods=['POST', 'GET'])
@login_required
def edit_user():
    """
    Edit current user
    User must be authorized to get access

    return edit_user  template with   EditUser form as paramtr in case of Get respone

    return redirect to main page with dept_id as paramtr in case of Post respone
    """
    form = EditUser(request.form)
    print(request.form)
    if request.method == "POST" and form.validate():
        if form.password.data == form.password_check.data:
            services.update_user(
                id=current_user.id,
                name=form.name.data,
                password=form.password.data
            )

        return redirect(url_for('main'))
    return render_template('edit_user.html', form=form)


@app.route('/show_payment/<int:personal_account_id>', methods=['GET'])
@login_required
def show_payment(personal_account_id: int):
    """
    Show page, where user can see employee
    User must be authorized to get access

    input parametrs:

    dep_id:int - id of departmnet, which user is atteched to

    return show_employee  template with  employees list and dep_id as paramtrs

    """
    global current_personal_account
    current_personal_account = personal_account_id
    payments = services.get_payment(int(personal_account_id))

    client_id = services.get_client_id(current_personal_account)

    return render_template('show_payment.html', payments=payments, client_id=client_id, current_aha=current_aha)


@app.route('/add_payment', methods=['POST', 'GET'])
@login_required
def add_payment():
    """
    Show page, where user can add employee
    User must be authorized to get access


    input parametrs:

    dep_id:int - id of departmnet, which user is atteched to

    return add_employee  template with  AddEmployeeForm form as paramtr in case of Get respone

    return redirect to show_employee template with dept_id as paramtr in case of Post respone
    """
    global current_personal_account
    form = AddPayment(request.form)
    if request.method == "POST" and form.validate():
        services.add_payment(
            personal_account_id=current_personal_account,
            total_price=form.total_price.data,
            date=form.date.data,
        )
        return redirect(url_for('show_payment', personal_account_id=current_personal_account))
    return render_template('add_payment.html', form=form)


@app.route('/show_meter/<int:personal_account_id>', methods=['GET'])
@login_required
def show_meter(personal_account_id: int):
    """
    Show page, where user can see employee
    User must be authorized to get access

    input parametrs:

    dep_id:int - id of departmnet, which user is atteched to

    return show_employee  template with  employees list and dep_id as paramtrs

    """
    global current_personal_account
    current_personal_account = personal_account_id
    meters = services.get_meter(int(personal_account_id))

    for i in range(len(meters)):
        meters[i].type = services.get_resource(meters[i].resourceRef)

    client_id = services.get_client_id(current_personal_account)

    return render_template('show_meter.html', meters=meters, client_id=client_id, current_aha=current_aha)


@app.route('/add_meter', methods=['POST', 'GET'])
@login_required
def add_meter():
    """
    Show page, where user can add employee
    User must be authorized to get access


    input parametrs:

    dep_id:int - id of departmnet, which user is atteched to

    return add_employee  template with  AddEmployeeForm form as paramtr in case of Get respone

    return redirect to show_employee template with dept_id as paramtr in case of Post respone
    """
    global current_personal_account
    form = AddMeter(request.form)
    if request.method == "POST" and form.validate():
        services.add_meter(
            serial_number=form.serial_number.data, be_active_to=form.be_active_to.data,
            personal_account_id=current_personal_account, resource_id=int(form.resource.data)
        )
        return redirect(url_for('show_meter', personal_account_id=current_personal_account))
    return render_template('add_meter.html', form=form)


@app.route("/edit_meter/<int:id>", methods=["GET", "POST"])
@login_required
def edit_meter(id: int):
    """
    Show page, where user can edit employee
    User must be authorized to get access


    input parametrs:

    dep_id:int - id of departmnet, which user is atteched to
    id:int - id of employee that will be edited

    return edit_employee  template with  AddEmployeeForm form as paramtr in case of Get respone

    return redirect to show_employee template with dept_id as paramtr in case of Post respone
    """
    form = AddMeter(request.form)
    global current_personal_account

    if request.method == "POST" and form.validate():
        services.edit_meter(
            id=id,
            serial_number=form.serial_number.data,
            be_active_to=form.be_active_to.data,
            personal_account_id=current_personal_account,
            resource_id=int(form.resource.data)
        )
        return redirect(url_for('show_meter', personal_account_id=current_personal_account))
    return render_template('edit_meter.html', form=form)


@app.route("/delete_meter/<int:id>", methods=["GET"])
@login_required
def delete_meter(id: int):
    """
    Delete employee
    User must be authorized to get access

    input parametrs:

    dep_id:int - id of departmnet, that will be deleted
    id:int - id of employee that will be deleted

    return redirect to main page
    """
    services.delete_meter(id)
    return redirect(url_for('show_meter', personal_account_id=current_personal_account))


@app.route('/show_meter_data/<int:meter_id>', methods=['GET'])
@login_required
def show_meter_data(meter_id: int):
    """
    Show page, where user can see employee
    User must be authorized to get access

    input parametrs:

    dep_id:int - id of departmnet, which user is atteched to

    return show_employee  template with  employees list and dep_id as paramtrs

    """

    meters_data = services.get_meter_date(int(meter_id))

    return render_template(
        'show_meter_data.html', meters_data=meters_data, personal_account_id=current_personal_account,
        meter_id=meter_id,current_aha=current_aha
    )


@app.route('/add_meter_data/<int:meter_id>', methods=['POST', 'GET'])
@login_required
def add_meter_data(meter_id):
    """
    Show page, where user can add employee
    User must be authorized to get access


    input parametrs:

    dep_id:int - id of departmnet, which user is atteched to

    return add_employee  template with  AddEmployeeForm form as paramtr in case of Get respone

    return redirect to show_employee template with dept_id as paramtr in case of Post respone
    """
    global current_personal_account
    form = AddMeterDate(request.form)
    form.coeficient.choices = [
        *zip([i.id for i in services.get_coeficient()], [i.value for i in services.get_coeficient()])]
    if request.method == "POST" and form.validate():
        services.add_meter_data(
            resource=form.resource.data,
            meter_id=meter_id,
            coeficient_id=form.coeficient.data,
            personal_account_id=current_personal_account
        )
        return redirect(url_for('show_meter_data', meter_id=meter_id))
    return render_template('add_meter_data.html', form=form)


@app.route('/show_bill/<int:personal_account_id>', methods=['GET'])
@login_required
def show_bill(personal_account_id: int):
    """
    Show page, where user can see employee
    User must be authorized to get access

    input parametrs:

    dep_id:int - id of departmnet, which user is atteched to

    return show_employee  template with  employees list and dep_id as paramtrs

    """
    global current_personal_account
    current_personal_account = personal_account_id
    bills = services.get_bill(int(personal_account_id))
    client_id = services.get_client_id(current_personal_account)
    return render_template(
        'show_bill.html', bills=bills, client_id=client_id, current_aha=current_aha
    )


@app.route('/edit_bill/<int:id>', methods=['GET', 'POST'])
@login_required
def edit_bill(id: int):
    """
    Show page, where user can see employee
    User must be authorized to get access

    input parametrs:

    dep_id:int - id of departmnet, which user is atteched to

    return show_employee  template with  employees list and dep_id as paramtrs

    """
    form = EditBill(request.form)
    global current_personal_account

    if request.method == "POST" and form.validate():
        services.edit_bill(
            id=id,
            status=form.status.data,
            date=form.date_sent.data
        )
        return redirect(url_for('show_bill', personal_account_id=current_personal_account))
    return render_template('edit_bill.html', form=form)


@app.route("/delete_bill/<int:id>", methods=["GET"])
@login_required
def delete_bill(id: int):
    """
    Delete employee
    User must be authorized to get access

    input parametrs:

    dep_id:int - id of departmnet, that will be deleted
    id:int - id of employee that will be deleted

    return redirect to main page
    """
    services.delete_bill_meter_data(id)
    return redirect(url_for('show_bill', personal_account_id=current_personal_account))


@app.route('/show_privilege/<int:personal_account_id>', methods=['GET'])
@login_required
def show_privilege(personal_account_id: int):
    """
    Show page, where user can see employee
    User must be authorized to get access

    input parametrs:

    dep_id:int - id of departmnet, which user is atteched to

    return show_employee  template with  employees list and dep_id as paramtrs

    """
    global current_personal_account
    current_personal_account = personal_account_id
    privileges = services.get_privilege(int(personal_account_id))
    client_id = services.get_client_id(current_personal_account)
    return render_template(
        'show_privilege.html', privileges=privileges, client_id=client_id, current_aha=current_aha
    )


@app.route('/add_privilege', methods=['POST', 'GET'])
@login_required
def add_privilege():
    """
    Show page, where user can add employee
    User must be authorized to get access


    input parametrs:

    dep_id:int - id of departmnet, which user is atteched to

    return add_employee  template with  AddEmployeeForm form as paramtr in case of Get respone

    return redirect to show_employee template with dept_id as paramtr in case of Post respone
    """
    global current_personal_account
    form = AddPrivilege(request.form)
    if request.method == "POST" and form.validate():
        services.add_privilege(
            water_discount=form.water_discount.data,
            energy_discount=form.energy_discount.data,
            personal_account_id=current_personal_account
        )
        return redirect(url_for('show_privilege', personal_account_id=current_personal_account))
    return render_template('add_privilege.html', form=form)


@app.route("/edit_privilege/<int:id>", methods=["GET", "POST"])
@login_required
def edit_privilege(id: int):
    """
    Show page, where user can edit employee
    User must be authorized to get access


    input parametrs:

    dep_id:int - id of departmnet, which user is atteched to
    id:int - id of employee that will be edited

    return edit_employee  template with  AddEmployeeForm form as paramtr in case of Get respone

    return redirect to show_employee template with dept_id as paramtr in case of Post respone
    """
    form = AddPrivilege(request.form)
    global current_personal_account

    if request.method == "POST" and form.validate():
        services.edit_privilege(
            id=id,
            water_discount=form.water_discount.data,
            energy_discount=form.energy_discount.data,
            personal_account_id=current_personal_account,
        )
        return redirect(url_for('show_privilege', personal_account_id=current_personal_account))
    return render_template('edit_privilege.html', form=form)


@app.route("/delete_privilege/<int:id>", methods=["GET"])
@login_required
def delete_privilege(id: int):
    """
    Delete employee
    User must be authorized to get access

    input parametrs:

    dep_id:int - id of departmnet, that will be deleted
    id:int - id of employee that will be deleted

    return redirect to main page
    """
    services.delete_privilege(id)
    return redirect(url_for('show_privilege', personal_account_id=current_personal_account))


@app.route('/show_contract/<int:client_id>', methods=['GET'])
@login_required
def show_contract(client_id: int):
    """
    Show page, where user can see employee
    User must be authorized to get access

    input parametrs:

    dep_id:int - id of departmnet, which user is atteched to

    return show_employee  template with  employees list and dep_id as paramtrs

    """
    contracts = services.get_contract(int(client_id))
    return render_template('show_contract.html', contracts=contracts, client_id=client_id, current_aha=current_aha)


@app.route("/edit_contract/<int:client_id>/<int:id>", methods=["GET", "POST"])
@login_required
def edit_contract(client_id: int, id: int):
    """
    Show page, where user can edit employee
    User must be authorized to get access


    input parametrs:

    dep_id:int - id of departmnet, which user is atteched to
    id:int - id of employee that will be edited

    return edit_employee  template with  AddEmployeeForm form as paramtr in case of Get respone

    return redirect to show_employee template with dept_id as paramtr in case of Post respone
    """
    form = AddContract(request.form)
    form.owner.choices = [
        *zip([i.id for i in services.get_owner()], [i.name+" "+i.surname for i in services.get_owner()])]
    if request.method == "POST" and form.validate():
        services.update_contract(
            id=id,
            date_of_start=form.date_of_start.data,
            date_of_end=form.date_of_end.data,
            owner_id=form.owner.data,
            client_id=client_id
        )
        return redirect(url_for('show_contract', client_id=client_id))
    return render_template('edit_contract.html', form=form)


@app.route('/add_contract/<int:client_id>', methods=['POST', 'GET'])
@login_required
def add_contract(client_id):
    """
    Show page, where user can add employee
    User must be authorized to get access


    input parametrs:

    dep_id:int - id of departmnet, which user is atteched to

    return add_employee  template with  AddEmployeeForm form as paramtr in case of Get respone

    return redirect to show_employee template with dept_id as paramtr in case of Post respone
    """
    form = AddContract(request.form)
    form.owner.choices = [
        *zip([i.id for i in services.get_owner()], [i.name + " " + i.surname for i in services.get_owner()])]
    if request.method == "POST" and form.validate():
        services.add_contract(
            date_of_start=form.date_of_start.data,
            date_of_end=form.date_of_end.data,
            owner_id=form.owner.data,
            client_id=client_id
        )
        return redirect(url_for('show_contract', client_id=client_id))
    return render_template('add_contract.html', form=form)


@app.route("/delete_contract/<int:client_id>/<int:id>", methods=["GET"])
@login_required
def delete_contract(client_id: int, id: int):
    """
    Delete employee
    User must be authorized to get access

    input parametrs:

    dep_id:int - id of departmnet, that will be deleted
    id:int - id of employee that will be deleted

    return redirect to main page
    """
    services.delete_contract(id)
    return redirect(url_for('show_contract', client_id=client_id))



