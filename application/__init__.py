from flask import Flask
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.ext.automap import automap_base
from sqlalchemy.orm import Session
from sqlalchemy import create_engine

server = 'DESKTOP-KGRQQMN'
database = 'CurseWork'
DATABSE_URI = 'mssql+pyodbc://' + server + '/' + database + '?trusted_connection=yes&driver=SQL+Server'

app = Flask(__name__)
Base = automap_base()


app.config['SQLALCHEMY_DATABASE_URI'] = DATABSE_URI
app.config['SECRET_KEY'] = 'secret'

db = SQLAlchemy(app)

engine = create_engine(DATABSE_URI)


Base.prepare(engine, reflect=True)

login_manager = LoginManager()
login_manager.init_app(app)

import application.views
