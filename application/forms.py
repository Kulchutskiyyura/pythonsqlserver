from wtforms import Form, BooleanField, StringField, FloatField, PasswordField, IntegerField, DateField, SelectField, \
    validators


class AddAHA(Form):
    name = StringField('Name', [validators.Length(min=3, max=25)])
    location = StringField('Location', [validators.Length(min=3, max=25)])


class AddClient(Form):
    name = StringField('Name', [validators.Length(min=3, max=25)])
    surname = StringField('Surname', [validators.Length(min=3, max=25)])
    address = StringField('Address', [validators.Length(min=3, max=125)])
    aha = SelectField('AHA', coerce=int)


class AddPersonalAcount(Form):
    address = StringField('Address', [validators.Length(min=3, max=125)])
    balance = FloatField('Balance')
    is_deptor = BooleanField('Debtor')
    client = SelectField('Client', coerce=int)


class AddMeter(Form):
    serial_number = IntegerField("Serial Number")
    be_active_to = DateField('Be Active To')
    resource = SelectField('Resource', choices=[(1, 'Water'), (2, 'Energy')])


class AddPayment(Form):
    total_price = FloatField('Price')
    date = DateField('Date')


class AddMeterDate(Form):
    resource = IntegerField('Amount')
    coeficient = SelectField('Coeficient', coerce=int)


class AddPrivilege(Form):
    water_discount = FloatField("Water discount")
    energy_discount = FloatField("Energy discount")


class AddContract(Form):
    date_of_start = DateField('Date of start')
    date_of_end = DateField('Date of end')
    owner = SelectField('Owner', coerce=int)


class AddEmployeeForm(Form):
    name = StringField('Name', [validators.Length(min=3, max=25)])
    surname = StringField('Surname', [validators.Length(min=3, max=25)])
    salary = IntegerField("Salary")
    date_of_birth = DateField("Date Of Birth")
    department = SelectField('Department', coerce=int)


class EditBill(Form):
    date_sent = DateField("Date of sent")
    status = SelectField('Resource', choices=[("new", 'new'), ("send", 'send'), ("verified", 'verified')])


class CreateDepartment(Form):
    name = StringField('Name', [validators.Length(min=3, max=25)])


class LogIn(Form):
    name = StringField('Name', [validators.Length(min=3, max=25)])
    password = PasswordField('Password', [validators.Length(min=3, max=25)])
    remember = BooleanField("Remember")


class CreateUser(Form):
    name = StringField('Name', [validators.Length(min=3, max=25)])
    password = PasswordField('Password', [validators.Length(min=3, max=25)])
    password_check = PasswordField('Repeat Password', [validators.Length(min=3, max=25)])
    access_level = SelectField('Access Level', choices=[(1, 'Standart'), (2, 'Admin')])


class EditUser(Form):
    name = StringField('Name', [validators.Length(min=3, max=25)])
    password = PasswordField('Password', [validators.Length(min=3, max=25)])
    password_check = PasswordField('Repeat Password', [validators.Length(min=3, max=25)])
