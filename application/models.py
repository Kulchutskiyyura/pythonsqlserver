from application import Base
from application import db
from flask_login import UserMixin


class User(UserMixin, db.Model):
    __tablename__ = 'user'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(25), nullable=False)
    surname = db.Column(db.String(25), nullable=False)
    login = db.Column(db.String(25), nullable=False)
    password = db.Column(db.String(25), nullable=False)

    access = {}

    def __repr__(self):
        return f"user: {self.name}, {self.password}, {self.access_level}"


Admin = Base.classes.Admin
AHA = Base.classes.AHA
AhaAccess = Base.classes.AhaAccess
Client = Base.classes.Client
Owner = Base.classes.Owner
Contract = Base.classes.Contract
PersonalAcount = Base.classes.PersonalAcount
Privilege = Base.classes.Privilege
Meter = Base.classes.Meter
Payment = Base.classes.Payment
Coeficient = Base.classes.Coeficient
Resource = Base.classes.Resource
Bill = Base.classes.Bill
MeterDate = Base.classes.MeterDate
