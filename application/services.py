from sqlalchemy import select
from sqlalchemy.orm import Session
from datetime import datetime

from application import db
from application import engine
from application.models import *


def get_admin_by_login(login: str, password: str):
    session = Session(engine)
    statement = session.query(Admin).filter_by(login="log", password="pass").first()
    user = None
    if statement is not None:
        user = User(
            id=statement.id,
            login=statement.login,
            password=statement.password,
            name=statement.name,
            surname=statement.surname
        )
    session.commit()

    return user


def get_admin_by_id(id: int):
    session = Session(engine)
    access = get_acceess(id)
    statement = session.query(Admin).filter_by(id=id).first()
    user = None
    if statement is not None:
        access = {i.ahaRef: i.accessValue for i in access}
        user = User(
            id=statement.id,
            login=statement.login,
            password=statement.password,
            name=statement.name,
            surname=statement.surname,
            access=access
        )
    session.commit()

    return user


def get_acceess(admin_id: int):
    session = Session(engine)
    statement = session.query(AhaAccess).filter_by(adminRef=admin_id).all()
    session.close()

    return statement


def get_ahas():
    session = Session(engine)
    statement = session.query(AHA).all()
    session.close()

    return statement


def add_aha(name: str, location: str):
    session = Session(engine)
    session.add(AHA(name=name, location=location))
    session.commit()


def edit_aha(id: int, name: str, location: str):
    session = Session(engine)
    session.query(AHA).filter_by(id=id).update({'name': name, 'location': location})
    session.commit()


def delete_aha(id: int):
    session = Session(engine)
    session.query(AHA).filter_by(id=id).delete()
    session.commit()


def get_client(aha_id: int):
    session = Session(engine)
    statement = session.query(Client).filter_by(ahaRef=aha_id).all()
    session.close()

    return statement


def get_all_client():
    session = Session(engine)
    statement = session.query(Client).all()
    session.close()

    return statement


def add_client(name: str, surname: str, address: str, aha_id: int):
    session = Session(engine)
    session.add(Client(name=name, surname=surname, realAdress=address, ahaRef=aha_id))
    session.commit()


def update_client(id: int, name: str, surname: str, address: str, aha_id: int):
    session = Session(engine)
    session.query(Client).filter_by(id=id).update(
        {'name': name, 'surname': surname, 'realAdress': address, 'ahaRef': aha_id})
    session.commit()


def delete_client(id: int):
    session = Session(engine)
    session.query(Client).filter_by(id=id).delete()
    session.commit()


def get_personal_account(client_id: int):
    session = Session(engine)
    statement = session.query(PersonalAcount).filter_by(clientRef=client_id).all()
    session.close()

    return statement


def add_personal_account(address: str, balance: int, is_deptor: bool, client_id: int):
    session = Session(engine)
    session.add(PersonalAcount(
        registrationAdress=address, balance=int(float(balance) * 100), isDeptor=is_deptor, clientRef=client_id)
    )
    session.commit()


def update_personal_account(id: int, address: str, balance: int, is_deptor: bool, client_id: int):
    session = Session(engine)
    session.query(PersonalAcount).filter_by(id=id).update(
        {'registrationAdress': address, 'balance': int(float(balance) * 100), 'isDeptor': is_deptor,
         'clientRef': client_id})
    session.commit()


def delete_personal_account(id: int):
    session = Session(engine)
    session.query(PersonalAcount).filter_by(id=id).delete()
    session.commit()


def get_client_id(personal_account_id: int):
    session = Session(engine)
    statement = session.query(PersonalAcount).filter_by(id=personal_account_id).first()
    session.close()

    return statement.clientRef


def get_payment(personal_account_id: int):
    session = Session(engine)
    statement = session.query(Payment).filter_by(personalAcountRef=personal_account_id).all()
    session.close()

    return statement


def add_payment(personal_account_id: int, total_price: int, date: datetime):
    session = Session(engine)
    total_price = int(float(total_price) * 100)

    session.add(Payment(
        totalPrice=total_price, data=date, personalAcountRef=personal_account_id)
    )

    record = statement = session.query(PersonalAcount).filter_by(id=personal_account_id).first()
    session.query(PersonalAcount).filter_by(id=personal_account_id).update(
        {'balance': record.balance + total_price})

    session.commit()


def get_resource(id: int):
    session = Session(engine)
    statement = session.query(Resource).filter_by(id=id).first()
    session.close()

    return statement.type


def get_meter(personal_account_id: int):
    session = Session(engine)
    statement = session.query(Meter).filter_by(personalAcountRef=personal_account_id).all()
    session.close()

    return statement


def add_meter(serial_number: int, be_active_to: datetime, personal_account_id: int, resource_id):
    session = Session(engine)
    session.add(Meter(serialNumber=serial_number, beActiveTo=be_active_to, personalAcountRef=personal_account_id,
                      resourceRef=resource_id))
    session.commit()


def edit_meter(id: int, serial_number: int, be_active_to: datetime, personal_account_id: int, resource_id):
    session = Session(engine)
    session.query(Meter).filter_by(id=id).update(
        {'serialNumber': serial_number, 'beActiveTo': be_active_to, 'resourceRef': resource_id,
         'personalAcountRef': personal_account_id})
    session.commit()


def delete_meter(id: int):
    session = Session(engine)
    session.query(Meter).filter_by(id=id).delete()
    session.commit()


def get_meter_date(meter_id: int):
    session = Session(engine)
    statement = session.query(MeterDate).filter_by(meterRef=meter_id).all()
    session.close()

    return statement


def get_meter_type(id: int):
    session = Session(engine)
    statement = session.query(Meter).filter_by(id=id).first()

    session.close()

    resource = get_resource(statement.resourceRef)

    return resource


def get_coeficient():
    session = Session(engine)
    statement = session.query(Coeficient).all()
    session.close()

    return statement


def add_bill(price: int, personal_account_id: int, meter_type: str):
    session = Session(engine)
    local_dt = datetime.now()
    price = int(price * 100)
    privilege = get_privilege(personal_account_id=personal_account_id)
    discount = 0
    if meter_type == "Energy":
        discount = sum([i.energyDiscount for i in privilege])
    else:
        discount = sum([i.waterDiscount for i in privilege])

    price = int(price - (float(price) * discount / 100))

    session.add(Bill(price=price, dateOfSent=local_dt, personalAcountRef=personal_account_id, status="new"))

    record = statement = session.query(PersonalAcount).filter_by(id=personal_account_id).first()
    session.query(PersonalAcount).filter_by(id=personal_account_id).update(
        {'balance': record.balance - price})

    session.commit()


def add_meter_data(resource: int, meter_id: int, coeficient_id: int, personal_account_id: int):
    session = Session(engine)
    local_dt = datetime.now()
    statement = session.query(Coeficient).filter_by(id=coeficient_id).first()

    meter_type = get_meter_type(meter_id)

    add_bill(price=resource * statement.value, personal_account_id=personal_account_id, meter_type=meter_type)
    statement = session.query(Bill).all()

    max_id = max(statement, key=lambda x: x.id)

    session.add(
        MeterDate(id=max_id.id, amountOfResource=resource, date=local_dt, meterRef=meter_id,
                  coeficientRef=coeficient_id))

    session.commit()


def get_bill(personal_account_id: int):
    session = Session(engine)
    statement = session.query(Bill).filter_by(personalAcountRef=personal_account_id).all()
    session.close()

    return statement


def edit_bill(id: int, date: datetime, status: str):
    session = Session(engine)
    session.query(Bill).filter_by(id=id).update(
        {'status': status, 'dateOfSent': date, })
    session.commit()


def delete_bill(id: int):
    session = Session(engine)
    session.query(Bill).filter_by(id=id).delete()
    session.commit()


def delete_meter_data(id: int):
    session = Session(engine)
    session.query(MeterDate).filter_by(id=id).delete()
    session.commit()


def delete_bill_meter_data(id: int):
    delete_meter_data(id)
    delete_bill(id)


def get_privilege(personal_account_id: int):
    session = Session(engine)
    statement = session.query(Privilege).filter_by(personalAcountRef=personal_account_id).all()
    session.close()

    return statement


def add_privilege(water_discount: int, energy_discount: int, personal_account_id: int):
    session = Session(engine)
    session.add(
        Privilege(waterDiscount=water_discount, energyDiscount=energy_discount, personalAcountRef=personal_account_id,
                  ))
    session.commit()


def edit_privilege(id: int, water_discount: int, energy_discount: int, personal_account_id: int):
    session = Session(engine)
    session.query(Privilege).filter_by(id=id).update(
        {'waterDiscount': water_discount, 'energyDiscount': energy_discount,
         'personalAcountRef': personal_account_id})
    session.commit()


def delete_privilege(id: int):
    session = Session(engine)
    session.query(Privilege).filter_by(id=id).delete()
    session.commit()


def get_contract(client_id: int):
    session = Session(engine)
    statement = session.query(Contract).filter_by(clientRef=client_id).all()
    session.close()

    return statement


def get_owner():
    session = Session(engine)
    statement = session.query(Owner).all()
    session.close()

    return statement


def update_contract(id: int, date_of_start: datetime, date_of_end: datetime, owner_id: int, client_id: int):
    session = Session(engine)
    session.query(Contract).filter_by(id=id).update(
        {'dataOfStart': date_of_start, 'dataOfEnd': date_of_end, 'clientRef': client_id,
         'ownerRef': owner_id})
    session.commit()


def add_contract(date_of_start: datetime, date_of_end: datetime, owner_id: int, client_id: int):
    session = Session(engine)
    session.add(
        Contract(dataOfStart=date_of_start, dataOfEnd=date_of_end, clientRef=client_id, ownerRef=owner_id
                 ))
    session.commit()


def delete_contract(id: int):
    session = Session(engine)
    session.query(Contract).filter_by(id=id).delete()
    session.commit()
